import React, {useState} from 'react'
import { AppBar, Box, Button, Grid, Modal, TextField, Toolbar, Typography } from '@mui/material'
import "../css/ouser.css"
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 300,
    bgcolor: 'background.paper',
    border: '60px solid #e4dbdb',
    boxShadow: 24,
    p: 4,
  };


export const Ousermyapp = () => {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [Nama, setNama] = useState("");
    const [Addres, setAddres] = useState("");
    const [Hobby, setHobby] = useState("");
    const [ArrayData, setArrayData] = useState([]);
    const [Submit, setSubmit] = useState(false);

    const handleuser =()=>{
        let datahandle = ArrayData ? ArrayData : []

        if(datahandle.length===0){
            return <Grid container direction='column' justifyContent='space-evenly' alignItems='center' >
                <Grid item xl={12} className='texto'>0</Grid>
                <Grid item xl={12} className='textu'>USER</Grid>
            </Grid>
        }
        else{
            return datahandle.map((value,index)=>{
                return <div className='boxuser'>
                    <div>
                        <div><h2>{value.nama}</h2></div>
                        <div>{value.address}</div>
                    </div>
                    <div>
                        <h2>{value.hobby}</h2>
                    </div>
                </div>
            })
        }

    }

    const Addarray =()=>{
        let datahandle = ArrayData ? ArrayData : []
        datahandle.push({nama:Nama, address:Addres, hobby:Hobby});
        setArrayData(datahandle);
        setSubmit(!Submit);
    }


  return (
    <div>
        <Grid container spacing={2}>
           
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
            <Toolbar>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          MY APP
            </Typography>
            <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px'}} onClick={handleOpen}>Add User</Button>
            </Toolbar>
            </AppBar>
        </Box>
    </Grid>
    <Grid container spacing={2}>
        <Grid item xl={12}>
            {handleuser()}
        </Grid>
    </Grid>
<Modal
  open={open}
  onClose={handleClose}
  aria-labelledby="modal-modal-title"
  aria-describedby="modal-modal-description"
>
  <Box sx={style}>
    <Grid className='modaladduser' container>
        <Grid item xl={12} textAlign='center'><h1>Add User</h1></Grid>
        <Grid item xl={12} className='textfield'>
        <TextField  required id='outlined-required' label='Nama' onChange={(e)=>{setNama(e.target.value)}} ></TextField>
        </Grid>
        <Grid item xl={12} className='textfield'>
        <TextField required id='outlined-required' label='Address' onChange={(e)=>{setAddres(e.target.value)}} ></TextField>
        </Grid>
        <Grid item xl={12} className='textfield'>
        <TextField required id='outlined-required' label='Hobby' onChange={(e)=>{setHobby(e.target.value)}} ></TextField>
        </Grid>
        <Button variant='contained' sx={{backgroundColor:'#00b7c8',borderRadius:'30px',width:'100px'}} onClick={()=>{Addarray()}} >Save</Button>
    </Grid>
    {/* <Typography id="modal-modal-title" variant="h6" component="h2">
      Text in a modal
    </Typography>
    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
      Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
    </Typography> */}
  </Box>
</Modal>
    </div>
  )
}
